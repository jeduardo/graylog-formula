{% from "graylog/map.jinja" import host_lookup as config with context %}

{% if config.graylog.install_java %}
# Install java from a package
package-install-java-openjdk:
  pkg.installed:
    - name: {{ config.package.java_pkg_name }}
{% endif %}
